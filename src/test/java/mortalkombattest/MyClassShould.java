package mortalkombattest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import rpgmortalkombat.Faction;
import rpgmortalkombat.Melee;
import rpgmortalkombat.Ranged;
import rpgmortalkombat.Tree;

public class MyClassShould {

	@Test
	public void createCharacter() {

		Melee character = new Melee();
		
		double result = 1000;
		
		assertEquals(result, character.getHealth(), 0.001);		
		assertEquals(true, character.isAlive());
		assertEquals(1, character.getLevel());
		
	}

	@Test
	public void takeDamage() {
		
		Melee character = new Melee();

		character.takeDamage(100);
		assertEquals(900, character.getHealth(), 0.001);

		character.takeDamage(900);
		assertEquals(0, character.getHealth(), 0.001);
		assertEquals(false, character.isAlive());
	}
	
	@Test
	public void takeHealth() {
		
		Melee character = new Melee();

		character.takeHealth(100);
		assertEquals(1000, character.getHealth(), 0.001);

		character.takeDamage(500);
		assertEquals(500, character.getHealth(), 0.001);
		
		character.takeHealth(100);
		assertEquals(600, character.getHealth(), 0.001);
		
		
	}
	
	@Test
	public void giveHealthSameFaction() throws Exception {
		
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		Faction faction = new Faction("creeper");
		
		characterA.addFaction(faction);
		characterB.addFaction(faction);

		characterA.giveHealth(100, characterB);
		assertEquals(1000, characterB.getHealth(), 0.001);

		characterB.takeDamage(500);
		assertEquals(500, characterB.getHealth(), 0.001);
		
		characterA.giveHealth(100, characterB);
		assertEquals(600, characterB.getHealth(), 0.001);
				
	}
	
	@Test
	public void giveHealthNotSameFaction() throws Exception {
		
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		Faction faction = new Faction("creeper");
		
		characterA.addFaction(faction);
		//characterB.addFaction(faction);

		characterA.giveHealth(100, characterB);
		assertEquals(1000, characterB.getHealth(), 0.001);

		characterB.takeDamage(500);
		assertEquals(500, characterB.getHealth(), 0.001);
		
		characterA.giveHealth(100, characterB);
		assertEquals(500, characterB.getHealth(), 0.001);
				
	}
	
	@Test
	public void isDead() {
		
		Melee character = new Melee();
		
		character.takeDamage(1000);
		assertEquals(0, character.getHealth(), 0.001);
		assertEquals(false, character.isAlive());
		
		character.takeHealth(100);
		assertEquals(0, character.getHealth(), 0.001);
		assertEquals(false, character.isAlive());

	}
	
	@Test
	public void attack() throws Exception {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		boolean correct = characterA.attack(10, characterA);
		
		assertEquals(false, correct);
		
		correct = characterA.attack(10, characterB);
		
		assertEquals(true, correct);
		
		assertEquals(990, characterB.getHealth(), 0.001);
		
		
	}
	
	@Test
	public void factionAttack() throws Exception {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		Faction faction = new Faction("creeper");
		
		characterA.addFaction(faction);
//		characterB.addFaction(faction);
		
		boolean correct = characterA.attack(10, characterB);
		assertEquals(true, correct);
	}
	
	
	@Test
	public void levelDifferenceIsMoreThan5betweenAtoB() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		characterB.setLevel(10);
		
		characterA.attack(100, characterB);
		
		assertEquals(950, characterB.getHealth(),0.001);
	}
	
	@Test
	public void levelDifferenceIsLessThan5betweenAtoB() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		characterB.setLevel(4);
		
		characterA.attack(100, characterB);
		
		assertEquals(900, characterB.getHealth(),0.001);
	}

	@Test
	public void levelDifferenceIsMoreThan5betweenBtoA() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		characterA.setLevel(10);
		
		characterA.attack(100, characterB);
		
		assertEquals(850, characterB.getHealth(),0.001);
	}
	
	@Test
	public void levelDifferenceIsLessThan5betweenBtoA() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		characterA.setLevel(4);
		
		characterA.attack(100, characterB);
		
		assertEquals(900, characterB.getHealth(),0.001);
	}
	
	@Test
	public void meleeAtackOutOfRange() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		characterA.setCoords(new double[]{1, 4});
		
		characterA.attack(100, characterB);
		assertEquals(1000, characterB.getHealth(),0.001);
	}

	
	@Test
	public void meleeAtackInRange() {
		Melee characterA = new Melee();
		Melee characterB = new Melee();
		
		characterA.setCoords(new double[]{1, 1});
		
		characterA.attack(100, characterB);
		assertEquals(900, characterB.getHealth(),0.001);
	}
	
	@Test
	public void rangedAtackOutOfRange() {
		Ranged characterA = new Ranged();
		Melee characterB = new Melee();
		
		characterA.setCoords(new double[]{-21, 0});
		
		characterA.attack(100, characterB);
		assertEquals(1000, characterB.getHealth(),0.001);
	}

	
	@Test
	public void rangedAtackInRange() {
		Ranged characterA = new Ranged();
		Melee characterB = new Melee();
		
		characterA.setCoords(new double[]{1, 1});
		
		characterA.attack(100, characterB);
		assertEquals(900, characterB.getHealth(),0.001);
	}

	@Test
	public void addToFacion() {
		Melee character = new Melee();
		character.addFaction(new Faction("CREEPER"));
		assertEquals("CREEPER", character.getFaction("CREEPER").getName());
	}

	@Test
	public void add2Facion() {
		Melee character = new Melee();
		character.addFaction(new Faction("CREEPER"));
		character.addFaction(new Faction("COUP"));
		assertEquals("CREEPER", character.getFaction("CREEPER").getName());
		assertEquals("COUP", character.getFaction("COUP").getName());
	}	
	
	@Test
	public void removeFaction() {
		Melee character = new Melee();
		character.addFaction(new Faction("CREEPER"));
		character.removeFaction("CREEPER");
		assertEquals(null, character.getFaction("CREEPER"));
	}
	
	@Test
	public void atackTree() {
		Tree tree = new Tree();
		
		tree.takeDamage(100);
		assertEquals(901, tree.getHealth(), 0.001);

		tree.takeDamage(901);
		assertEquals(0, tree.getHealth(), 0.001);
		assertEquals(false, tree.isAlive());
		
	}
	
	@Test(expected = Exception.class)
	public void healTree() throws Exception {
		Melee melee = new Melee();
		Tree tree = new Tree();
		
//		Faction faction = new Faction("creeper");
//		
//		melee.addFaction(faction);
//		tree.addFaction(faction);
		
		
		melee.giveHealth(100, tree);
		
		
	}
	
}
