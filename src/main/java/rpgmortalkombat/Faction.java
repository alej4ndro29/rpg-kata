package rpgmortalkombat;

public class Faction {

	private String name;
	
	public Faction(String name) {
		this.name = name;
	}
	
	public Faction() {
		name = "Unnamed";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Faction [name=").append(name).append("]");
		return builder.toString();
	}
	
	
	
}
