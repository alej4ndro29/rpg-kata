package rpgmortalkombat;

import java.util.HashMap;
import java.util.Map;

public abstract class Entity {

	protected double health;
	protected int level;
	protected boolean alive;
	protected double[] coords;
	protected boolean animated = true; 
	
	protected Map<String, Faction> userFactions;

	protected double attackRange;

	public Entity() {
		health = 1000;
		level = 1;
		alive = true;
		coords = new double[] { 0, 0 };

		userFactions = new HashMap<>();
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(double health) {
		this.health = health;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public double[] getCoords() {
		return coords;
	}

	public void setCoords(double[] coords) {
		this.coords = coords;
	}

	public double getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(double attackRange) {
		this.attackRange = attackRange;
	}

	public void takeDamage(double damage) {
		if (health > 0) {
			health -= damage;

			if (health <= 0) {
				alive = false;
				health = 0;
			}
		}
	}
	
	public abstract void giveHealth(double heal, Entity character) throws Exception;
	
	public abstract void takeHealth(double health) throws Exception;
//	public void takeHealth(double health) {
//		if (health > 0) {
//			if (isAlive()) {
//				this.health += health;
//
//				if (this.health > 1000) {
//					this.health = 1000;
//				}
//			}
//		}
//
//	}

	public boolean attack(double damage, Entity character) {
		if (canAttack(damage, character)) {
			return false;
		}

		double maxRange = character.getAttackRange();

		double xa = coords[0];
		double ya = coords[1];

		double xb = character.getCoords()[0];
		double yb = character.getCoords()[1];

		double xdiff = Math.abs(xa - xb);
		double ydiff = Math.abs(ya - yb);

		if (xdiff < maxRange && ydiff < maxRange) {
			if (character.getLevel() > level + 4) {
				damage = damage * 0.50;
			} else if (character.getLevel() + 4 < level) {
				damage = damage * 1.50;
			}
			character.takeDamage(damage);
			return true;
		} else {
			return false;
		}

	}

	protected boolean canHeal(double heal, Entity character) {
		
		if(heal > 0) {
			for (Map.Entry<String, Faction> entry: userFactions.entrySet()) {
			    if(character.getFaction(entry.getValue().getName()) != null) {
			    	return true;
			    }
			}
		} 
		
		return false;

	}
	
	protected boolean canAttack(double damage, Entity character) {
		
		if(character.equals(this) || damage < 0) {
			return true;
		}
		
		for (Map.Entry<String, Faction> entry: userFactions.entrySet()) {
		    if(character.getFaction(entry.getValue().getName()) != null) {
		    	return true;
		    }
		}
		
		return false;

	}

	public void addFaction(Faction faction) {

		if (!factionExistsInUserList(faction)) {
			userFactions.put(faction.getName(), faction);
		}
	}

	public void removeFaction(String faction) {
		userFactions.remove(faction);
	}

	public Faction getFaction(String faction) {
		return userFactions.get(faction);
	}

	protected boolean factionExistsInUserList(Faction faction) {
		Faction result = userFactions.get(faction.getName());
		return result != null;

	};
	
}
