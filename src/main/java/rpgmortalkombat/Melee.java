package rpgmortalkombat;

public class Melee extends Entity {

	public Melee() {
		super();
		this.attackRange = 2;
	}

	@Override
	public void giveHealth(double heal, Entity character) throws Exception {
		if(canHeal(heal, character)) {
			character.takeHealth(heal);
		}
	}

	@Override
	public void takeHealth(double health) {
		if (health > 0) {
			if (isAlive()) {
				this.health += health;

				if (this.health > 1000) {
					this.health = 1000;
				}
			}
		}

	}
}
